Plug 'maralla/completor.vim'

let g:completor_filetype_map = {}
let g:completor_filetype_map.c = {'ft': 'lsp', 'cmd': 'clangd-8'}
let g:completor_filetype_map.cpp = {'ft': 'lsp', 'cmd': 'clangd-8'}
