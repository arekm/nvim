Plug 'natebosch/vim-lsc'
Plug 'ajh17/VimCompletesMe'

set shortmess-=F
let g:lsc_enable_autocomplete  = v:true
let g:lsc_enable_diagnostics   = v:true
let g:lsc_reference_highlights = v:false
let g:lsc_trace_level          = 'off'
let g:lsc_hover_popup          = v:true

let g:lsc_auto_completeopt='menu,menuone,noinsert,noselect'
autocmd CompleteDone * silent! pclose

let g:lsc_server_commands = {
 \  'cpp': {
 \    'command': 'clangd-8',
 \    'suppress_stderr': v:true
 \  },
 \  'c': {
 \    'command': 'clangd-8',
 \    'suppress_stderr': v:true
 \  }
 \}

let g:lsc_auto_map = {
 \  'GoToDefinition': 'gd',
 \  'FindReferences': 'gr',
 \  'Rename': 'gR',
 \  'ShowHover': 'K',
 \  'Completion': 'completefunc',
 \}
 "\  'Completion': 'omnifunc',
 
 
