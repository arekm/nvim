
"inoremap <buffer> "" ""<left>
"inoremap <buffer> '' ''<left>
"inoremap <buffer> () ()<left>
"inoremap <buffer> [] []<left>
"inoremap <buffer> {} {}<left>
"inoremap <buffer> <> <><left>

"inoremap <buffer> <tab>' ''<left>
"inoremap <buffer> <tab>" ""<left>
"inoremap <buffer> <tab>( ()<left>
"inoremap <buffer> <tab>) ();<left><left>
"inoremap <buffer> <tab>[ []<left>
"inoremap <buffer> <tab>< <><left>

"inoremap <buffer> <tab>{ {<cr>}<cr><Esc>k<S-o>
"inoremap <buffer> <tab>} {<cr>};<cr><Esc>k<S-o>
""
"inoremap <buffer> <tab>s struct<space><space>{<cr>};<cr><Esc>kk^f<space>a
"inoremap <buffer> <tab>c class<space><space>{<cr>};<cr><Esc>kk^f<space>a
"inoremap <buffer> <tab>f for<space>()<space>{<cr>}<cr><Esc>kk^f(a
"inoremap <buffer> <tab>i if<space>()<space>{<cr>}<cr><Esc>kk^f(a
"inoremap <buffer> <tab>w while<space>()<space>{<cr>}<cr><Esc>kk^f(a
"inoremap <buffer> <tab>v void<space>()<space>{<cr>}<cr><Esc>kk^t(a
"inoremap <buffer> <tab>t /*<space>TODO:<space><space>*/<Esc>hhi
"inoremap <buffer> <tab>n /*<space>NOTE:<space><space>*/<Esc>hhi
"inoremap <buffer> <tab>/ /*<space><space>*/<Esc>hhi
