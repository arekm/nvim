set nocompatible
set path+=**
set wildmenu

set breakindent
set linebreak

set expandtab
set tabstop=4
set softtabstop=4
set shiftwidth=4
set relativenumber
set number
"set showmatch
set ignorecase
set smarttab
set hlsearch
set visualbell           " don't beep
set noerrorbells         " don't beep
set nobackup
set noswapfile
set laststatus=2
set nopaste
set background=light
let mapleader = "\\"
"set cursorcolumn
"
" Light theme settings
hi CursorLine  cterm=NONE ctermbg=lightyellow
autocmd InsertEnter * highlight  CursorLine ctermbg=none ctermfg=None
autocmd InsertLeave * highlight  CursorLine ctermbg=lightyellow ctermfg=None
hi CursorLineNR  cterm=NONE ctermbg=white ctermfg=black
hi PmenuSel cterm=NONE ctermbg=black ctermfg=white
hi Pmenu cterm=NONE ctermbg=white ctermfg=black

" Dark theme settings
"hi CursorLine  cterm=NONE ctermbg=black
"autocmd InsertEnter * highlight  CursorLine ctermbg=none ctermfg=None
"autocmd InsertLeave * highlight  CursorLine ctermbg=black ctermfg=None

"hi CursorLineNR  cterm=NONE ctermbg=black ctermfg=white
"autocmd InsertEnter * highlight  CursorLineNR ctermbg=none ctermfg=None
"autocmd InsertLeave * highlight  CursorLineNR ctermbg=black ctermfg=None
"
"hi CursorColumn  cterm=NONE ctermbg=lightyellow 
"hi CursorLine  cterm=NONE ctermbg=lightgrey ctermfg=darkgrey
"hi CursorColumn  cterm=NONE ctermbg=lightgrey ctermfg=darkgrey
hi Search cterm=NONE ctermbg=blue ctermfg=white
hi IncSearch cterm=NONE ctermbg=blue ctermfg=white
hi SpellBad cterm=NONE ctermbg=red ctermfg=black
hi MatchParen cterm=NONE ctermbg=darkgrey ctermfg=lightgrey
hi Error ctermfg=red 
"hi Directory guifg=#FF0000 ctermfg=red
set cursorline
"set guicursor=a:blinkon1
set guicursor=
set nospell
set spelllang=pl

set showcmd  
set hidden

set nopaste
set so=999

syntax enable
filetype plugin on

call plug#begin('~/.config/nvim/plugged')

Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'

source ~/.config/nvim/deoplete.vim

Plug 'tpope/vim-fugitive'
Plug 'itchyny/lightline.vim'

Plug 'airblade/vim-gitgutter'
Plug 'majutsushi/tagbar'
Plug 'tpope/vim-surround'
Plug 'Chiel92/vim-autoformat'


" Nerdtree
map <C-t> :NERDTreeToggle<CR>

" git-gutter configuration

" Window management mappings

:nmap <silent> <C-h> :wincmd h<CR>
:nmap <silent> <C-j> :wincmd j<CR>
:nmap <silent> <C-k> :wincmd k<CR>
:nmap <silent> <C-l> :wincmd l<CR>

" Terminal windows managment maippins
:tnoremap <C-h> <C-\><C-N><C-w>h
:tnoremap <C-j> <C-\><C-N><C-w>j
:tnoremap <C-k> <C-\><C-N><C-w>k
:tnoremap <C-l> <C-\><C-N><C-w>l
:tnoremap <leader>\ <C-\><C-N>

" Tagbar configuration

nmap <F9> :TagbarOpen fjc<CR>
nmap <F8> :TagbarToggle<CR>

":nnoremap gw :grep -Iir <cword> .<CR><CR>:copen<CR>


" Ultisnips configuration
"

Plug 'SirVer/ultisnips'
"Plug 'honza/vim-snippets'
let g:UltiSnipsExpandTrigger="<tab>"
let g:UltiSnipsJumpForwardTrigger="<c-l>"
let g:UltiSnipsJumpBackwardTrigger="<c-k>"

" fzf.vim configuration -------------------------------------------
"
Plug '~/.fzf'
Plug 'junegunn/fzf.vim'

let g:fzf_tags_command = 'ctags -R --extra=+q'
let g:fzf_buffers_jump = 0

" Use Neovim floating window

let $FZF_DEFAULT_OPTS='--layout=reverse'
let g:fzf_layout = { 'window': 'call FloatingFZF()' }

function! FloatingFZF()
  let buf = nvim_create_buf(v:false, v:true)
  call setbufvar(buf, '&signcolumn', 'no')

  let height = &lines - 3
  let width = float2nr(&columns - (&columns * 2 / 10))
  let col = float2nr((&columns - width) / 2)

  let opts = {
        \ 'relative': 'editor',
        \ 'row': 1,
        \ 'col': col,
        \ 'width': width,
        \ 'height': height
        \ }

  call nvim_open_win(buf, v:true, opts)
endfunction

highlight NormalFloat cterm=NONE ctermfg=white ctermbg=black gui=NONE guifg=yellow guibg=green

" alternate.vim
"

Plug 'vim-scripts/a.vim'

"------------------------------------------------------------------
nmap <silent> <leader>t :term<CR>
nmap <silent> <leader>b :Buffers<CR>
nmap <silent> <leader>f :Files<CR>
nmap <silent> <leader><space> :Files<CR>
nmap <silent> <leader>u :BTags<CR>
nmap <silent> <leader>c :q<CR>
nmap <silent> <leader>l :BLines<CR>
nmap <silent> <leader>* :BLines <C-R><C-W><CR>
nmap <silent> <leader>m :Marks<CR>
nmap <leader>s :Ag <C-R><C-W><CR>
nmap <silent> <leader>w :w <CR>
nmap <silent> <leader>h :NERDTreeFind<CR>
nmap <silent> <leader>r :e ./<CR>
nmap <silent> <leader>g :grep -Iir <cword> .<CR><CR>:copen<CR>
nmap <silent> <leader>1 1<c-w>w
nmap <silent> <leader>2 2<c-w>w
nmap <silent> <leader>3 3<c-w>w
nmap <silent> <leader>4 4<c-w>w
nmap <silent> <leader>5 5<c-w>w
nmap <silent> <leader>6 6<c-w>w
nmap <silent> <leader>7 7<c-w>w
nmap <silent> <leader>8 8<c-w>w
nmap <silent> <leader>9 9<c-w>w
nmap <silent> <C-\> <ESC>
imap <silent> <C-\> <ESC>

nmap <silent> <leader>d i<CR><Esc>f<Space>

inoremap <leader><return> <Esc>o
inoremap <leader>\ <Esc>
inoremap <leader>a <Esc><S-A>

noremap <leader>\ <Esc>

map q <Nop>

hi MatchParen cterm=none ctermbg=green ctermfg=blue

Plug 'ncm2/float-preview.nvim'
let g:float_preview#docked = 0
set completeopt-=preview

call plug#end()

noremap <silent> k gk
noremap <silent> j gj
noremap <silent> 0 g0
noremap <silent> $ g$
nnoremap <Space> i


" White characters
set listchars=tab:▸-▸,eol:~,space:·
hi NonText cterm=NONE ctermbg=none ctermfg=grey
hi whitespace cterm=NONE ctermbg=none ctermfg=grey
set nolist

