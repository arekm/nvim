Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

Plug 'autozimu/LanguageClient-neovim', {
    \ 'branch': 'next',
    \ 'do': 'bash install.sh',
    \ }

let g:deoplete#enable_at_startup = 1

let g:LanguageClient_serverCommands = {
  \ 'cpp': ['clangd-9'],
  \ 'cxx': ['clangd-9'],
  \ 'c': ['clangd-9'],
  \ 'h': ['clangd-9'],
  \ 'hpp': ['clangd-9'],
  \ 'hxx': ['clangd-9'],
  \ }

